FROM php:7.3.11-apache

RUN a2enmod rewrite
WORKDIR /var/www/html
COPY ./src .