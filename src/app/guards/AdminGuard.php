<?php 

class AdminGuard
{
    public static function guard()
    {
        if(!Auth::user()->isAdmin())
        {
            return redirect('/logout');
        }
        else
        {
            return true;
        }
    }
}