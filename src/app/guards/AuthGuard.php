<?php 

class AuthGuard
{
    public static function guard()
    {
        if(isset($_SESSION['userId']))
        {
            return true;
        }
        else
        {
            return redirect('/login');
        }
    }
}