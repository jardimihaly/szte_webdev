<?php 
require __APPDIR__ . '/controllers/Auth.php';
require __APPDIR__ . '/controllers/Site.php';
require __APPDIR__ . '/controllers/Comment.php';
require __APPDIR__ . '/controllers/ContentController.php';
require __APPDIR__ . '/guards/AuthGuard.php';
require __APPDIR__ . '/guards/AdminGuard.php';

if($_POST)
{
    switch($_GET['path'])
    {
        case 'login':
            Auth::login();
            break;

        case 'register':
            Auth::register();
            break;

        case 'change-password':
            AuthGuard::guard();
            Auth::changePassword();
            break;

        case 'comment':
            AuthGuard::guard();
            CommentController::postComment();
            break;

        case 'remove-comment':
            AuthGuard::guard();
            CommentController::removeComment();
            break;

        case 'edit/tutorial':
            AuthGuard::guard();
            AdminGuard::guard();
            ContentController::updateContent('tutorial');
            break;

        case 'edit/about-godot':
            AuthGuard::guard();
            AdminGuard::guard();
            ContentController::updateContent('about-godot');
            break;

        default: 
            Site::notFound();
        break;
    }
}
else
{
    switch($_GET['path'])
    {
        case '':
        case '/':
            Site::index();
            break;

        case 'login':
            Site::login();
            break;

        case 'register':
            Site::register();
            break;

        case 'tutorial':
            Site::displayContent('tutorial');
            break;

        case 'about-godot':
            Site::displayContent('about-godot');
            break;

        case 'change-password':
            AuthGuard::guard();
            Site::changePassword();
        break;

        case 'logout':
            Auth::logout();
            break;

        case 'edit-sites':
            AuthGuard::guard();
            AdminGuard::guard();
            ContentController::editSites();
            break;

        default:
            Site::notFound();
        break;
    }
}

require __DIR__ . '/views/Layout.php';
    