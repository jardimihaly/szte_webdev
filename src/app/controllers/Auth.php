<?php 

class Auth
{
    static function login()
    {
        if(!isset($_POST['username'], $_POST['password']) && 
            strlen($_POST['username']) == 0 ||
            strlen($_POST['password']) == 0)
        {
            return redirect('/login&login-error');
        }

        if($user = User::userExists($_POST['username'], $_POST['password']))
        {
            $_SESSION['userId'] = $user->getId();
            return redirect('/');
        }
        else
        {
            return redirect('/login&credentials');
        }
    }

    static function register()  
    {
        if(!isset($_POST['username'], $_POST['password'], $_POST['password_confirm'], $_POST['email'], $_POST['sex'], $_POST['terms_of_use']) && 
            strlen($_POST['username']) == 0 ||
            strlen($_POST['password']) == 0 ||
            strlen($_POST['password']) == 0)
        {
            return redirect('/register&register-missing-error');
        }

        if($_POST['password'] != $_POST['password_confirm'] ||
            $_POST['terms_of_use'] != 'on')
        {
            return redirect('/register&register-invalid-error');
        }

        try
        {
            User::registerUser($_POST['username'], $_POST['password'], $_POST['email']);

            Auth::login();
        } 
        catch (\Throwable $th)
        {
            return redirect('/register&register-invalid-user-error');
        }
    }

    static function changePassword()
    {
        if(!isset($_POST['old_password'], $_POST['password'], $_POST['password_confirm']))
        {
            return redirect('/change-password&missing-error');
        }

        if(!User::getById($_SESSION["userId"])->checkPassword($_POST['old_password']))
            return redirect('/change-password&bad-password');

        if($_POST['password'] !== $_POST['password_confirm'])
        {
            return redirect('/change-password&missing-error');
        }

        User::getById($_SESSION["userId"])->changePassword($_POST['password']);

        return redirect('/change-password&succes');
    }

    static function loggedIn()
    {
        return isset($_SESSION['userId']);
    }

    static function logout()
    {
        session_destroy();

        return redirect('/login');
    }

    static function user()
    {
        return User::getById($_SESSION['userId']);
    }
}

