<?php

class ContentController
{
    public static function editSites()
    {
        return view('ContentEditor', [
            "tutorial" => Content::getSiteByName('tutorial'),
            "aboutGodot" => Content::getSiteByName('about-godot')
        ]);
    }

    public static function updateContent($site)
    {
        if(isset($_POST['content']))
        {
            Content::setSiteByName($site, $_POST['content']);
        }
        
        return redirect('/edit-sites');
    }
}