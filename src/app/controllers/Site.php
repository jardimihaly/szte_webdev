<?php 

class Site
{
    static function index()
    {
        return view('Index');
    }
    
    static function login()
    {
        return view('Login');
    }

    static function register()
    {
        return view('Register');
    }

    static function changePassword()
    {
        return view('change-password');
    }

    static function notFound()
    {
        return view('404');
    }

    static function displayContent($site)
    {
        return view('Content', ["site" => Content::getSiteByName($site)]);
    }
}