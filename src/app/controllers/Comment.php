<?php

class CommentController
{
    public static function postComment()
    {
        if(isset($_POST['siteName'], $_POST['comment']) &&
            strlen($_POST['siteName']) != 0 &&
            strlen($_POST['comment']) != 0)
        {
            Comment::addComment(
                $_POST['siteName'],
                User::getById($_SESSION['userId']),
                $_POST['comment']
            );
        }

        return redirect('/' . $_POST['siteName']);
    }

    public static function removeComment()
    {
        if(isset($_POST['commentId']))
        {
            Comment::removeComment($_POST['commentId']);
        }

        return redirect('/' . $_POST['siteName']);
    }
}