<?php
    $IMPORT_FILE;

    function redirect($to)
    {
        header("Location: $to");
        exit;
    }

    function view($name, $params = null)
    {
        global $IMPORT_FILE; 
        $IMPORT_FILE = __BASEDIR__ . "/app/views/$name.php";

        if($params && is_array($params))
        {
            foreach ($params as $var => $value) {
                global $$var;
                $$var = $value;
            }
        }
    }