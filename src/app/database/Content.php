<?php 

define("CONTENT_DIR",  __BASEDIR__ . '/data/content');

class Content
{
    static function init()
    {
        if(!file_exists(__BASEDIR__ . '/data'))
        {
            mkdir(__BASEDIR__ . '/data');
        }

        if(!file_exists(__BASEDIR__ . '/data/content'))
        {
            mkdir(__BASEDIR__ . '/data/content');
        }
    }

    static function getSiteByName($siteName)
    {
        $filePath = CONTENT_DIR . "/$siteName.html";

        if(!file_exists($filePath))
        {
            throw new Exception('Site does not exist!');
        }

        $site = new Content();
        $site->content = file_get_contents($filePath);
        $site->name = $siteName;
        $site->comments = Comment::getCommentsForSite($siteName);

        return $site;
    }

    static function setSiteByName($site, $contents)
    {
        $filePath = CONTENT_DIR . "/$site.html";

        file_put_contents($filePath, $contents);
    }

    private $name;

    public function getName()
    {
        return $this->name;
    }

    private $content;

    public function getContent()
    {
        return $this->content;
    }

    private $comments;

    public function getComments()
    {
        return $this->comments;
    }
}