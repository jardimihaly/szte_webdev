<?php 
require __APPDIR__ . '/database/User.php';
require __APPDIR__ . '/database/Comment.php';
require __APPDIR__ . '/database/Content.php';

User::init();
Content::init();
Comment::init();