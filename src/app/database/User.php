<?php 

define("USER_FILE",  __BASEDIR__ . '/data/user.csv');

class User
{
    static function init()
    {
        if(!file_exists(__BASEDIR__ . '/data'))
        {
            mkdir(__BASEDIR__ . '/data');
        }

        if(!file_exists(USER_FILE))
        {
            $contents = '1;admin;' . User::hashPw('admin') . ";admin@thissite.com;1\n";
            $fh = fopen(USER_FILE, 'w');
            if(!$fh)
            {
                die('Cannot open user file');
            }
            fprintf($fh, "%s", $contents);
            fclose($fh);
        }
    }

    private static function getUsers()
    {
        $lines = file(USER_FILE);
        $users = array();

        for($i = 0; $i < count($lines); $i++)
        {
            $contents = explode(';', $lines[$i]);
            $users[$i] = new User();
            $users[$i]->id = $contents[0];
            $users[$i]->username = $contents[1];
            $users[$i]->password = $contents[2];
            $users[$i]->email = $contents[3];
            $users[$i]->isAdmin = $contents[4];
        }

        return $users;
    }

    private static function saveUsers($users)
    {
        $lines = array();
        for($i = 0; $i < count($users); $i++)
        {
            $lines[$i] = $users[$i]->id
            . ";" . 
            $users[$i]->username
            . ";" . 
            $users[$i]->password
            . ";" . 
            $users[$i]->email
            . ";" .
            $users[$i]->isAdmin . "\n" ;
        }

        $fh = fopen(USER_FILE, "w");
        foreach($lines as $line)
        {
            fprintf($fh, "%s", $line);
        }
        fclose($fh);
    }

    public static function getById($id)
    {
        foreach (User::getUsers() as $user)
        {
            if($user->id === $id)
            {
                return $user;
            }
        }

        return NULL;
    }

    private static function hashPw($pass)
    {
        return hash('sha512', $pass);
    }

    private $id;

    function getId()
    {
        return $this->id;
    }

    private $username;

    function getUsername()
    {
        return $this->username;
    }

    private $password;
    private $email;
    private $isAdmin;

    function isAdmin()
    {
        return $this->isAdmin;
    }

    function checkPassword($password)
    {
        return User::hashPw($password) === $this->password;
    }

    function changePassword($newPassword)
    {
        $this->password = User::hashPw($newPassword);

        $users = User::getUsers();

        foreach($users as $user)
        {
            if($user->id === $_SESSION["userId"])
            {
                $user->password = $this->password;
                break;
            }
        }

        User::saveUsers($users);
    }

    static function registerUser($username, $password, $email)
    {
        $users = User::getUsers();

        foreach ($users as $user) 
        {
            if($user->username === $username || $user->email == $email)
            {
                throw new Exception('Felhasználó már létezik.');
            }
        }

        $user = new User();
        $user->id = $users[count($users) - 1]->id + 1;
        $user->username = $username;
        $user->password = User::hashPw($password);
        $user->email = $email;
        $user->isAdmin = false;

        $users[count($users)] = $user;
        User::saveUsers($users);
    }

    static function userExists($username, $password)
    {
        $users = User::getUsers();

        $hash = User::hashPw($password);

        foreach ($users as $user) 
        {
            if($user->username === $username && $user->password === $hash)
            {
                return $user;
            }
        }

        return false;
    }
}