<?php 

define("COMMENT_FILE",  __BASEDIR__ . '/data/comment.csv');

class Comment
{
    static function init()
    {
        if(!file_exists(__BASEDIR__ . '/data'))
        {
            mkdir(__BASEDIR__ . '/data');
        }

        if(!file_exists(COMMENT_FILE))
        {
            $fh = fopen(COMMENT_FILE, 'w');
            fclose($fh);
        }
    }

    public static function getComments()
    {
        $lines = file(COMMENT_FILE);
        $comments = array();
        $ci = 0;
        for($i = 0; $i < count($lines); $i++)
        {
            $contents = explode(';', $lines[$i]);
            if(count($contents) > 1)
            {
                $comments[$ci] = new Comment();
                $comments[$ci]->id = $contents[0];
                $comments[$ci]->site = $contents[1];
                $comments[$ci]->user = User::getById($contents[2]);
                $comments[$ci++]->text = $contents[3];
            }
        }

        $comments = array_reverse($comments);

        return $comments;
    }

    public static function saveComments($comments)
    {
        $lines = array();
        for($i = 0; $i < count($comments); $i++)
        {
            $lines[$i] = $comments[$i]->id . ';' .
            $comments[$i]->site . ';' .
            $comments[$i]->user->getId() . ';' .
            str_replace('\r', '', str_replace('\n', '', $comments[$i]->text)) . "\n";
        }

        $fh = fopen(COMMENT_FILE, "w");
        foreach($lines as $line)
        {
            fprintf($fh, "%s", $line);
        }
        fclose($fh);
    }

    public static function getCommentsForSite($site)
    {
        $allComments = Comment::getComments();
        $filteredComments = array();

        $ci = 0;
        for ($i=0; $i < count($allComments); $i++) { 
            if($allComments[$i]->site === $site)
            {
                $filteredComments[$ci++] = $allComments[$i];
            }
        }

        return $filteredComments;
    }

    public static function addComment($site, $user, $text)
    {
        $comments = Comment::getComments();

        $comment = new Comment();
        $comment->id = count($comments) > 0 ? $comments[count($comments) - 1]->id + 1 : 1;
        $comment->site = $site;
        $comment->user = $user;
        $comment->text = $text;

        $comments[count($comments)] = $comment;
        Comment::saveComments($comments);
    }

    public static function removeComment($commentId)
    {
        $comments = Comment::getComments();
        $remainingComments = array();

        $rci = 0;
        for ($i=0; $i < count($comments); $i++) 
        { 
            if($comments[$i]->id != $commentId)
            {
                $remainingComments[$rci++] = $comments[$i];
            }
        }

        Comment::saveComments($remainingComments);
    }

    private $id = '';

    public function getId()
    {
        return $this->id;
    }

    private $site = '';

    public function getSite()
    {
        return $this->site;
    }

    private $user = '';
    
    public function getUser()
    {
        return $this->user;
    }

    private $text = '';

    public function getText()
    {
        return $this->text;
    }
    
}