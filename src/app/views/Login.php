<div class="card">
    <h3 class="mb-3">Bejelentkezés</h3>
    <?php 
        if(isset($_GET['login-error']))
        {
            ?>
            <div class="text-center">
                <div class="alert alert-danger">
                    HIBA: Felhasználónév és jelszó megadása kötelező!
                </div>
            </div>
            
            <?php
        }

        if(isset($_GET['credentials']))
        {
            ?>
            <div class="text-center">
                <div class="alert alert-danger">
                    HIBA: Hibás bejelentkező adatok!
                </div>
            </div>
            <?php
        }
    ?>
    <div class="card-body">
        <form action="/login" method="post">
            <div class="form-group">
                <label for="username">Felhasználónév</label>
                <input type="text" name="username" id="username" class="input">
            </div>
            <div class="form-group">
                <label for="password">Jelszó</label>
                <input type="password" name="password" id="password" class="input">
            </div>
            <div class="content-center">
                <input type="submit" value="Bejelentkezés" class="button button-primary mb-2"><br>
                <a href="forgot.html" class="small">Elfelejtett jelszó?</a>
            </div>
        </form>
    </div>
</div>