<div class="card">
    <div class="card-body">
        <form action="/change-password" method = "POST">
            <div class="form-group">
                <label for="password">Régi Jelszó</label>
                <input type="password" name="old_password" id="old_password" class="input">
            </div>
            <div class="form-group">
                <label for="password">Jelszó</label>
                <input type="password" name="password" id="password" class="input">
            </div>
            <div class="form-group">
                <label for="password_confirm">Jelszó megerősítés</label>
                <input type="password" name="password_confirm" id="password_confirm" class="input">
            </div>
            <div class="content-center">
                <input type="submit" value="Mentés" class="button button-primary mb-2">
            </div>
        </form>
    </div>
</div>