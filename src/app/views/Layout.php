<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="resources/style/app.css">
    <link rel="shortcut icon" href="/favicon.png" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed&display=swap" rel="stylesheet">
    <title>Godot Tutorials</title>
</head>
<body>
    <?php 
        include 'Menu.php';
    ?>
    <div class="container pt-6">
        <?php 
            include $IMPORT_FILE;
        ?>
    </div>
</body>