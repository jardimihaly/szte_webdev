<?php 
?>

<form action="/edit/tutorial" method="post">
    <div class="form-group">
        <label for="content">Tutorial tartalma</label>
        <textarea name="content" id="content" class="input" rows="80"><?php echo $tutorial->getContent(); ?></textarea>
    </div>

    <div class="form-group">
        <input type="submit" value="Mentés" class="button button-accept">
    </div>
</form>

<form action="/edit/about-godot" method="post">
<div class="form-group">
        <label for="content">About-Godot tartalma</label>
        <textarea name="content" id="content" class="input" rows="80"><?php echo $aboutGodot->getContent(); ?></textarea>
    </div>

    <div class="form-group">
        <input type="submit" value="Mentés" class="button button-accept">
    </div>
</form>
