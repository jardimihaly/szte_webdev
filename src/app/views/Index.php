<div class="card">
    <div class="card-body">
    <h1 class="mb-5">Hírek</h1>
    <div class="card">
        <div class="card-body">
            <h2 class="mb-5">Vulkan API</h2>
            <p class="mb-3">A godot 4.0-ás verziója már támogatni fogja a Vulkán API-t. 
                Ez jó hír hiszen a Vulkan sokkal jobban használja ki a processzor magokat, mint az OpenGL.</p>
        </div>
    </div>

    <div class="card mt-5">
        <div class="card-body">
            <h2 class="mb-5">EPIC MEGAGRANT</h2>
            <p class="mb-3"> A godot kapott $250 000-t nyíltforráskódú grafikus szoftver kategóriában. </p>
            <a href="https://godotengine.org/article/godot-engine-was-awarded-epic-megagrant">Bővebben</a>
        </div>
    </div>

    <div class="card mt-5">
        <div class="card-body">
            <h2 class="mb-5">Godot 3.2</h2>
            <p class="mb-3"> Megjelent a godot 3.2-es verziója. </p>
            <a href="https://godotengine.org/article/here-comes-godot-3-2">Bővebben</a>
        </div>
    </div>

    <div class="card mt-5">
        <div class="card-body">
            <h2 class="mb-5">Steam</h2>
            <p class="mb-3"> Godot 3.1 már elérhető Steam-en is. </p>
            <a href="https://steamcommunity.com/games/404790/announcements/detail/1809790964539253946">Bővebben</a>
        </div>
    </div>
</div>