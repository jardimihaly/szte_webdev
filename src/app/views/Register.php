<div class="card">
    <div class="card-body">
        <form action="/register" method="POST">
            <div class="form-group">
                <label for="username">Felhasználónév</label>
                <input type="text" name="username" id="username" class="input">
            </div>
            <div class="form-group">
                <label for="sex">Nem</label> 
                <select class="input">
                    <option value="female">Nő</option>
                    <option value="male">Férfi</option>
                </select>
            </div>
            <div class="form-group">
                <label for="password">Jelszó</label>
                <input type="password" name="password" id="password" class="input">
            </div>
            <div class="form-group">
                <label for="password_confirm">Jelszó megerősítés</label>
                <input type="password" name="password_confirm" id="password_confirm" class="input">
            </div>
            <div class="form-group">
                <label for="email">Email cím</label>
                <input type="email" name="email" id="email" class="input">
            </div>
            <div class="form-group">
                <input class="f-left" type="checkbox" name="terms_of_use" id="terms_of_use">
                <label class="ml-4" for="terms_of_use"> Elfogadom a Felhasználási feltételeket.</label>
            </div>
            <div class="content-center">
                <input type="submit" value="Regisztráció" class="button button-primary mb-2">
                <div class="small d-block">
                    vagy <a href="login.html">bejelentkezés</a>
                </div>
            </div>
        </form>
    </div>
        </div>