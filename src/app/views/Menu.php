<div class="menu w-100">
    <nav class="navbar">
        <a href="index.html" class="navbar-logo">
            <img src="resources/images/godot-logo.svg" alt="Godot Logo" class="godot-logo">
            - Godot Tutorials
        </a>
        
        <ul class="navbar-nav">
            <a href="/" class="navbar-item">Főoldal</a>
            <a href="/about-godot" class="navbar-item">A Godotról</a>
            <a href="/tutorial" class="navbar-item">Tutorial</a>
            <?php if(Auth::loggedIn()): ?>
                <a href="/change-password" class="navbar-item">Jelszócsere</a>
                <a href="/logout" class="navbar-item">Kijelentkezés</a>
                <?php if(Auth::user()->isAdmin()): ?>
                    <a href="/edit-sites" class="navbar-item">Tartalom szerkesztése</a>
                <?php endif; ?>
            <?php else: ?>
                <a href="/login" class="navbar-item">Bejelentkezés</a>
                <a href="/register" class="navbar-item">Regisztráció</a>
            <?php endif; ?>
        </ul>
    </nav>
</div>