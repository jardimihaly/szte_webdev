<div class="p-5">
    <hr>
    <h3>
        <?php echo $comment->getUser()->getUsername() ?> 
        <?php if(Auth::user() && (Auth::user()->isAdmin() || Auth::user()->getId() == $_SESSION['userId'])): ?>
            <form action="/remove-comment" method="post" class="remove-comment">
                <input type="hidden" name="commentId" value="<?php echo $comment->getId(); ?>">
                <input type="hidden" name="siteName" value="<?php echo $comment->getSite(); ?>">
                <input type="submit" value="X"></input>
            </form>
        <?php endif; ?>
    </h3>
    
    <hr>
    <div class="p-2">
        
        <div>
            <?php echo $comment->getText() ?>
        </div>
    </div>
</div>