<hr class="mt-3" />

<h3 class="mb-3 mt-3">Kommentek</h3>

<?php if(Auth::loggedIn()): ?>

<form action="/comment" method="post">
    <input type="hidden" name="siteName" value="<?php echo $site->getName(); ?>">
    <div class="form-group">
        <textarea class="input" name="comment" id="comment" placeholder="Írj kommentet!"></textarea>
    </div>
    <div class="content-right">
        <input type="submit" value="Elküld" class="button button-primary mb-2">
    </div>
</form>    

<?php else: ?>
    <a href="/login">Jelentkezz be a kommenteléshez!</a>
<?php endif; ?>

<?php 
    foreach ($site->getComments() as $comment) {
        include 'Comment.php';
    }
?>

